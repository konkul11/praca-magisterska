﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace PracaMagisterska.Enums
{
    public enum Decyzja
    {
        [Description("Rzuć")]
        Rzuc = 1,
        Strzel = 2
    }
}