﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace PracaMagisterska.Enums
{
    public enum PlecGracza
    {
        [Description("Mężczyzna")]
        Mezczyzna = 1,

        Kobieta = 2
    }
}