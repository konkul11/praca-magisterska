﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PracaMagisterska.Enums
{
    public enum RolaUzytkownika
    {
        Administrator = 1,
        Uzytkownik = 2
    }
}