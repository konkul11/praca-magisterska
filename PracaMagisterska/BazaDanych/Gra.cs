//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PracaMagisterska.BazaDanych
{
    using System;
    using System.Collections.Generic;
    
    public partial class Gra
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Gra()
        {
            this.UczestnicyGry = new HashSet<UczestnikGry>();
        }
    
        public long Id { get; set; }
        public byte Typ { get; set; }
        public string Miejsce { get; set; }
        public bool CzyUsuniete { get; set; }
        public System.DateTime Data { get; set; }
        public long UzytkownikId { get; set; }
        public byte KategoriaWiekowaGry { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UczestnikGry> UczestnicyGry { get; set; }
        public virtual Uzytkownik Uzytkownik { get; set; }
    }
}
